let button = document.getElementById("button");
const inputin = document.getElementById("input");
let words = document.getElementById("words");
const redText = document.querySelector(".red-text");

let count = 0;
let count1 = 0;

function game() {
    let seconds = 61;
    function tick() {
        let timmer = document.getElementById("timer");
        seconds--;
        timmer.innerHTML = seconds + "s";
        if (seconds > 0) {
            setTimeout(tick, 1000);
        }
    }
    tick();

    async function showWord() {
        redText.innerHTML = await getWord();
    }

    async function answers() {
        for (let i = 0; i < redText.innerHTML.length; i++) {
            if (words.value == redText.innerHTML) {
                count1++;
            }
        }
        if (words.value == redText.innerHTML) {
            redText.innerHTML = await getWord();
            words.value = "";
            count++;
        }
        if (seconds == 0) {
            redText.innerHTML = `Je typte ${count} woorden (${count1} karakters) per minuut`;
            words.value = "";
            words.style.display = "none";
            button.style.display = "block";
        }
    }

    showWord();
    words.addEventListener("input", answers);
    words.style.display = "block";
    button.style.display = "none";
}
button.addEventListener("click", game);


/**
 * Use this function to retrieve a random word.
 *
 * @returns a string containing a random word.
 */
async function getWord() {
    const response = await fetch("https://random-word-bit.vercel.app/word");
    const word = JSON.parse(await response.text());
    return word[0].word.toLowerCase();
}
