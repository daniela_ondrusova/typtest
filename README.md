# Typsnelheidstest

**Features**

- De gebruiker krijgt 1 minuut om zo veel mogelijk woorden te typen.
- Na een minuut ziet de gebruiker het aantal getypte woorden en het aantal karakters per minuut.
- Alleen correct gespelde woorden tellen mee voor het totaal.


 Typetest kunt u **[hier](https://thunderous-bunny-b5fe96.netlify.app/)** vinden.